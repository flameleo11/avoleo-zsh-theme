
my_module() {
  function my_function { 
    # local time=" %D - %* "; 
    # local time=" %F - %T "; 
    local time=$(date +"%Y-%m-%d %H:%M:%S")
    echo "$C1${time}000$LR"
  }
}

# Call the module

local ver="7644"
function r { 
  source ~/.zshrc
  echo "$ver"
}

RPROMPT="%B${return_code}%b"
RPROMPT=$(
  function func1 { 
    echo "111"
  }
  function func2 { 
    echo "222"
  }

  my_module && my_function;
)
xxx=$(
  function func1 { 
    echo "111"
  }
  function func2 { 
    echo "222"
  }

  my_module && my_function;
)
echo $xxx;
